local deployment = import "../../releng/hugo-websites/kube-deployment.jsonnet";

deployment.newProductionDeploymentWithStaging(
  "openresearchsw.eclipse.org", "openresearchsw-staging.eclipse.org"
)